package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String value;

    public Message(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}