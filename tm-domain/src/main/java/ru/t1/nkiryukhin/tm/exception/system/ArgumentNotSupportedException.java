package ru.t1.nkiryukhin.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
