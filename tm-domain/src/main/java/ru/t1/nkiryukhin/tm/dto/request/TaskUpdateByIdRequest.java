package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    public TaskUpdateByIdRequest(@Nullable final String token) {
        super(token);
    }

}
