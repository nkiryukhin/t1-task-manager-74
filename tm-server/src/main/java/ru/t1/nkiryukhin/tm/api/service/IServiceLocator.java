package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.service.dto.IProjectDTOService;
import ru.t1.nkiryukhin.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.nkiryukhin.tm.api.service.dto.ITaskDTOService;
import ru.t1.nkiryukhin.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IUserDTOService getUserService();

}
