package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    @NotNull
    List<Project> findByUserId(@NotNull final String userId);

    @NotNull
    List<Project> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<Project> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}