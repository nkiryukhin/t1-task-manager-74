package ru.t1.nkiryukhin.tm.listener.database;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

@Component
public final class DatabaseCreateListener extends AbstractDatabaseListener {

    public static final String NAME = "database-create";

    @NotNull
    @Override
    public String getDescription() {
        return "Create database";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@databaseCreateListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATABASE CREATING]");
        @NotNull final DatabaseCreateRequest request = new DatabaseCreateRequest();
        adminEndpoint.createDatabase(request);
        loggerService.info("DATABASE WAS SUCCESSFULLY CREATED");
    }

}
