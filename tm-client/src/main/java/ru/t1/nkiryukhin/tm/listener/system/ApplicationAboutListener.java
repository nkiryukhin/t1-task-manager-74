package ru.t1.nkiryukhin.tm.listener.system;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.ApplicationAboutRequest;
import ru.t1.nkiryukhin.tm.dto.response.ApplicationAboutResponse;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);

        System.out.println("AUTHOR: " + response.getName());
        System.out.println("E-MAIL: " + response.getEmail());
        System.out.println();
    }

}